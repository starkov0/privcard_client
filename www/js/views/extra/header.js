window.HeaderView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('header'));
		this.logoutModel = new Logout();
		this.authentication = '';
	},

	events: {
		"click #logout": "logout"
	},

	logout: function() {
		this.logoutModel.save(null, {
			success: function(model, response) {
				console.log('success: HeaderView-logout')
				MyStorage.storeAuthenticationType('')
				MyStorage.storePassword('')
				MyStorage.storeToken('')
				Backbone.history.navigate('account', true)
			},
			error: function(model, response) {
				console.log('error: HeaderView-logout')
				console.log(response)
			}
		})
	},

	setAccount: function() {
		this.authentication = 'account';
		this.render()
	},
	setCard: function() {
		this.authentication = 'card';
		this.render()
	},
	setAnonymous: function() {
		this.authentication = '';
		this.render()
	},

	render: function() {
		$(this.el).html(this.template({
			authentication: this.authentication
		}));
		return this;
	}

});
