window.LoginView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('login'));
		$(document).on('keydown', this.input_validation);
		this.tmpUsername = '';
		this.tmpPassword = '';
		this.showAlert = false
	},

	events: {
		'click #login': 'login'
	},

	login: function() {
		this.input_validation();
		if ($('form').valid()) {
			this.login_post();
		}
	},

	login_post: function() {
		var that = this;
		this.tmpUsername = $('#username').val();
		this.tmpPassword = $('#password').val();
		var login = new Login({
			username: this.tmpUsername,
			password: MyCrypto.generate_hash(this.tmpPassword),
			passwork_key: ''
		});
		login.save(null, {
			success: function(model, response) {
				console.log('success: LoginView-save-login')
				MyStorage.storePassword(that.tmpPassword)
				MyStorage.storeToken(response.token)
				if ('account' in response) {
					MyStorage.storeAuthenticationType('account')
				} else if ('card' in response) {
					MyStorage.storeAuthenticationType('card')
				}
				Backbone.history.navigate('', true)
			},
			error : function (model, response) {
				console.log('error: LoginView-save-login');
				console.log(response)
				that.showAlert = true
				that.render()
			}
		})
	},

	input_validation: function() {
		$('form').validate({
			rules: {
				username: {
					minlength: 1,
					required: true
				},password: {
					minlength: 8,
					required: true
				}
			},
			highlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				$(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
			},
			unhighlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				$(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.length) {
					error.insertAfter(element);
				} else {
					error.insertAfter(element);
				}
			}
		});
	},

	render: function() {
		$(this.el).html(this.template({
			tmpUsername: this.tmpUsername,
			tmpPassword: this.tmpPassword,
			showAlert: this.showAlert
		}));
		return this;
	}

});
