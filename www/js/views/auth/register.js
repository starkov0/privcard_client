window.RegisterView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('register'));
		$(document).on('keyup', this.input_validation);
		this.tmpUsername = '';
		this.tmpPassword = '';
		this.tmpPasswordAgain = '';
		this.showAlert = false;
	},

	events: {
		'click #register': 'register'
	},

	register: function() {
		this.input_validation();
		if ($('form').valid()) {
			this.register_post();
		}
	},

	register_post: function() {
		var that = this;
		that.tmpUsername = $('#username').val();
		that.tmpPassword = $('#password').val();
		that.tmpPasswordAgain = $('#password_again').val();
		var register = new Register({
			username: that.tmpUsername,
			password: MyCrypto.generate_hash(that.tmpPassword),
			password_key: MyCrypto.encrypt(MyRandomGenerator.generate_random_string(22), that.tmpPassword)
		});
		register.save(null, {
			success: function(model, response) {
				console.log('success: RegisterView-save-register')
				that.showAlert = false;
				MyStorage.storeAuthenticationType('account')
				MyStorage.storeToken(response.token)
				MyStorage.storePassword($('#password').val())
				Backbone.history.navigate('', true)
			},
			error : function (model, response) {
				console.log('error: RegisterView-save-register');
				console.log(response)
				that.showAlert = true
				that.render()
			}
		})
	},

	input_validation: function() {
		var rules = {
			username: {
				minlength: 1,
				required: true
			},password: {
				minlength: 8,
				required: true
			},password_again: {
				equalTo: "#password",
				required: false
			}
		};
		$('form').validate({
			rules: rules,
			highlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				$(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
			},
			unhighlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				$(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.length) {
					error.insertAfter(element);
				} else {
					error.insertAfter(element);
				}
			}
		});
	},

	render: function() {
		$(this.el).html(this.template({
			tmpUsername: this.tmpUsername,
			tmpPassword: this.tmpPassword,
			tmpPasswordAgain: this.tmpPasswordAgain,
			showAlert: this.showAlert
		}));
		return this;
	}

});
