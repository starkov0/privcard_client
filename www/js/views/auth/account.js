window.AccountView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('account'));
		$(document).on('keyup', $.proxy(this.input_validation,this));
		$(document).on('keyup', $.proxy(this.save_validation,this));
		this.account = options.account;
		this.key = MyCrypto.decrypt(this.account.get('password_key'), MyStorage.fetchPassword())
		this.update_password = false;
		this.tmpUsername = this.account.get('username');
		this.tmpPassword = '';
		this.tmpPasswordAgain = '';
		this.tmpStoredPassword = '';
		this.showAlert = false;
	},

	events: {
		'click #password_button': 'password_button',
		'click #update_button': 'update_button'
	},

	storeUsername: function() {
		this.tmpUsername = $('#username').val();
	},

	storePassword: function() {
		this.tmpPassword = $('#password').val();
		this.tmpPasswordAgain = $('#password_again').val();
	},

	resetPassword: function() {
		this.tmpPassword = ''
		this.tmpPasswordAgain = '';
	},

	// password action button
	password_button: function() {
		this.storeUsername()
		if (this.update_password) {
			this.storePassword()
		}
		this.update_password = !this.update_password;
		this.render();
		this.save_validation();
	},

	// update action button
	update_button: function() {
		this.input_validation();
		if ($('form').valid()) {
			this.storeUsername();
			this.update_account();
			this.update_post();
		}
	},

	// locally update account
	update_account: function() {
		this.account.set({'username': $('#username').val()})
		if (this.update_password) {
			this.tmpStoredPassword = $('#password').val();
			this.account.set('password_key', MyCrypto.encrypt(this.key, this.tmpStoredPassword))
			this.account.set('password', MyCrypto.generate_hash(this.tmpStoredPassword))
		}
	},

	// update account on server
	update_post: function() {
		var that = this;
		this.account.save(null, {
			type: 'PUT',
			success: function(model, response) {
				console.log('success: AccountView-update-Account')
				MyStorage.storePassword(that.tmpStoredPassword)
				that.showAlert = false;
				that.update_password = false;
				that.resetPassword()
				that.render();
				that.save_validation();
			},
			error: function(model, response) {
				console.log('error: AccountView-update-Account')
				console.log(response);
				that.showAlert = true;
				that.render();
				that.save_validation();
			}
		})
	},

	//  check if need to save
	save_validation: function() {
		var need_to_save = false;
		// username
		if ($('#username').val() != this.account.get('username')) {
			need_to_save = true;
		}
		// password
		if (this.update_password && ($('#password').val() ||$('#password_again').val())) {
			need_to_save = true;
		}
		// showAlert
		if (this.showAlert) {
			need_to_save = true;
		}
		var id_attr = "#update_button";
		if (need_to_save) {
			$(id_attr).removeClass('btn-success').addClass('btn-warning');
		} else {
			$(id_attr).removeClass('btn-warning').addClass('btn-success');
		}
	},

	input_validation: function() {
		var rules = {
			username: {
				minlength: 1,
				required: true
			},password: {
				minlength: 8,
				required: true
			},password_again: {
				equalTo: "#password",
				required: true
			}
		};
		$('form').validate({
			rules: rules,
			highlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				$(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
			},
			unhighlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				$(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.length) {
					error.insertAfter(element);
				} else {
					error.insertAfter(element);
				}
			}
		});
	},

	render: function() {
		$(this.el).html(this.template({
			account: this.account,
			update_password: this.update_password,
			tmpUsername: this.tmpUsername,
			tmpPassword: this.tmpPassword,
			tmpPasswordAgain: this.tmpPasswordAgain,
			showAlert: this.showAlert
		}));
		return this;
	}

});
