window.SQuestionListView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('squestion/main'));
		this.template_list = _.template(tpl.get('squestion/list'));
		$(document).on('keydown', $.proxy(this.input_validation,this));
		this.account = options.account;
		this.squestion_list = [];
		this.get_squestion_list();
	},

	events: {
		'click #add_squestion': 'add_squestion',
		'click #delete_squestion': 'delete_squestion'
	},

	add_squestion: function() {
		this.input_validation();
		if ($('form').valid()) {
			this.add_squestion_post();
		}
	},

	add_squestion_post: function() {
		var that = this;
		var question = $('#question').val();
		var answer = $('#answer').val();
		var key = MyCrypto.decrypt(this.account.get('password_key'), MyStorage.fetchPassword())
		var squestion = new SQuestion({
			id: null,
			question: question,
			answer: MyCrypto.generate_hash(answer),
			answer_key: MyCrypto.encrypt(key, answer)
		});
		squestion.save(null, {
			success: function(model, response) {
				console.log('success: SQuestionListView-save-SQuestion');
				that.get_squestion_list()
			},
			error : function (model, response) {
				console.log('error: SQuestionListView-save-SQuestion');
				console.log(response)
			}
		});
	},

	delete_squestion: function(event) {
		var that = this;
		var index = $(event.currentTarget).attr('name');
		var squestion = this.squestion_list[index];
		squestion.destroy({
			success: function(model, response) {
				console.log('success: QuestionView-delete-SQuestion');
				that.get_squestion_list()
			},
			error: function(model, response) {
				console.log('error: QuestionView-delete-SQuestion');
				console.log(response)
			}
		})
	},

	get_squestion_list: function() {
		var that = this;
		this.squestion_list = [];
		var key = MyCrypto.decrypt(this.account.get('password_key'), MyStorage.fetchPassword())
		new SQuestion().fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: QuestionView-fetch-SQuestion')
				for (var i=0; i<response.length; i++) {
					that.squestion_list.push(new SQuestion(response[i]))
				}
				that.render()
			},
			error: function(model, response) {
				console.log('error: QuestionView-fetch-SQuestion')
				console.log(response)
			}
		})
	},

	input_validation: function() {
		var rules = {
			question: {
				minlength: 1,
				required: true
			},answer: {
				minlength: 8,
				required: true
			},answer_again: {
				equalTo: "#answer",
				minlength: 8,
				required: true
			}
		};
		$('form').validate({
			rules: rules,
			highlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				$(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
			},
			unhighlight: function(element) {
				var id_attr = "#" + $( element ).attr("id") + "1";
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				$(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.length) {
					error.insertAfter(element);
				} else {
					error.insertAfter(element);
				}
			}
		});
	},

	render: function() {
		$(this.el).html(this.template({}));
		this.render_list();
		return this;
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			squestion_list: this.squestion_list
		}));
	}

});
