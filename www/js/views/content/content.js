window.ContentView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('category/content'));
		this.template_list = _.template(tpl.get('category/content_list'));
		this.template_bar = _.template(tpl.get('category/content_bar'));
		this.template_selected = _.template(tpl.get('category/content_selected'));
		this.template_search = _.template(tpl.get('share/search'));

		// event
		//$(document).on('keyup', $.proxy(this.search,this));

		// ok
		this.key = options.key
		this.pk_category = options.pk_category;

		// selected files
		this.selected_files = [];

		// directory
		this.direcotry_list = [];

		// filecontainer
		this.global_criterion = 0;
		this.filecontainer_list = [];
		this.sub_filecontainer_list = [];
		this.uploading = false;
		this.organized_files = []

		// progress bar
		this.read_progress_show = true;
		this.save_progress_show = true;
		this.read_progress_completion = 0;
		this.save_progress_completion = 0;

		//this.get_filecontainer_list()

		//this.get_directory_list()

		//this.render_graph()
	},

	events: {
		"click #upload_file_list": "upload_file_list",
		"click #stop_uploading": "stop_uploading",
		"click #delete_filecontainer": "delete_filecontainer",
		"change #file_select": "file_select",
		"change #directory_select": "directory_select",
		"click #create_directory": "create_directory"
	},


	//////////////////////////////////////////////////////////////// UPLOAD DIRECTORY
	create_directory: function () {
		var that = this
		var name = MyCrypto.encrypt('New Directory', this.key)
		var directory = new Directory({
			id: null,
			name: name,
			category: new Category({id: this.pk_category}),
			head_directory: [],
			filecontainer_set: []
		})
		directory.save(null, {
			success: function(model, response) {
				console.log('success: CategoryView-save-Directory')
				directory.set('name', MyCrypto.decrypt(directory.get('name'), that.key))
				that.direcotry_list.push(directory)
				that.render_list()
			},
			error: function(model, response) {
				console.log('error: CategoryView-save-Directory')
				console.log(response)
			}
		})
	},

	get_directory_list: function () {
		var that = this;
		that.direcotry_list = [];
		var tmp_directory = new Directory({
			id: null,
			name: '',
			category: new Category({id: this.pk_category}),
			head_directory: [],
			filecontainer_set: []
		})
		tmp_directory.fetch({
			type: 'GET',
			success: function (model, response) {
				console.log('success: ContainerView-fetch-Directory')
				for (var i=0; i<response.length; i++) {
					var directory = new Directory(response[i])
					directory.set('name', MyCrypto.decrypt(directory.get('name'), that.key))
					that.direcotry_list.push(directory);
				}
				that.render_list()
			},
			error: function (model, response) {
				console.log('error: ContainerView-fetch-Directory')
				console.log(response)
			}
		});
	},

	//////////////////////////////////////////////////////////////// UPLOAD FILE
	file_select: function () {
		var files = $('#file_select').prop('files');
		for (var i=0; i<files.length; i++) {
			this.selected_files.push(files[i]);
		}
		this.render_selected()
	},

	directory_select: function () {
		var files = $('#directory_select').prop('files');
		for (var i=0; i<files.length; i++) {
			this.selected_files.push(files[i]);
		}
		this.render_selected()
	},

	upload_file_list: function() {
		this.global_criterion = 1;
		this.upload_filecontainer_list()
	},

	stop_uploading: function () {
		this.global_criterion = 0;
		this.selected_files = [];
		this.read_progress_completion = 0;
		this.save_progress_completion = 0;
		this.render_selected()
	},

	upload_filecontainer_list: function() {
		if (this.selected_files.length && !this.uploading) {
			this.uploading = true;
			var file = this.selected_files.shift();
			this.render_selected()
			this.read_file(file);
		}
	},

	read_file: function(file) {
		var that = this;
		var reader = new FileReader();
		this.read_progress_completion = 0;
		this.save_progress_completion = 0;
		reader.readAsBinaryString(file);

		// read
		reader.onprogress = function(data) {
			if (!that.global_criterion) {
				reader.abort()
			}
			if (data.lengthComputable) {
				that.read_progress_completion = Math.round(data.loaded / data.total * 100);
				that.render_bar()
			}
		};

		// save
		reader.onload = function() {
			if (that.global_criterion) {
				that.post_filecontainer(file.name, file.webkitRelativePath, reader.result);
			}
		};
	},

	post_filecontainer: function (name, path, file) {
		var that = this;
		var category = new Category({id: this.pk_category, filecontainer_set: []});
		var name_enc = MyCrypto.encrypt(name, this.key);
		var path_enc = MyCrypto.encrypt(path, this.key);
		var filecontainer = new FileContainer({id: null, name: name_enc, path: path_enc, category: category, fileindex_set: []});

		filecontainer.save(null, {
			success: function(model, response) {
				console.log('success: CategoryView-save-FileContainer')
				if (that.global_criterion) {
					filecontainer.set({name: MyCrypto.decrypt(filecontainer.get('name'), that.key)})
					filecontainer.set({path: MyCrypto.decrypt(filecontainer.get('path'), that.key)})
					that.filecontainer_list.push(filecontainer)
					that.render_list()
					that.post_fileindex(file, filecontainer.id)
				}
			},
			error: function(model, response) {
				console.log('error: CategoryView-save-FileContainer')
				console.log(response)
			}
		})
	},

	post_fileindex: function(file, filecontainer_id) {
		var that = this;
		var category = new Category({id: this.pk_category, filecontainer_set: []});
		var filecontainer = new FileContainer({id: filecontainer_id, name: '', path: '', category: category, fileindex_set:[]});
		var fileindex_length = 300000;
		var progression = 0;
		var index = 0;
		var local_criterion = 1;

		//init thread
		var t = weaver.thread();
		t.require('./js/utils/myCrypto.js')
		t.require('./lib/sjcl.js')
		t.require('./lib/lz-string.min.js')

		// encrypt + compress
		t.run(function(){
			listen(function( msg ){
				msg.subfile = MyCrypto.encrypt(msg.subfile, msg.key);
				msg.subfile = LZString.compress(msg.subfile);
				broadcast( msg );
			});
		});

		// send fileindex to server
		t.on('message', function( e ){
			// check global creterion
			if (!that.global_criterion) {
				t.stop()
			}

			var msg = e.message;
			var fileindex = new FileIndex({
				id: null,
				index: msg.index,
				file: msg.subfile,
				category: category,
				filecontainer: filecontainer
			});
			fileindex.save(null, {
				success: function (model, response) {
					console.log('success: CategoryView-save-FileIndex')

					// progression
					progression += msg.subfile_len
					that.save_progress_completion = Math.round(progression/file.length*100);
					that.render_bar()

					// check global criterion
					if (!that.global_criterion) {
						t.stop()
					} else {
						if (progression == file.length) {
							that.uploading = false;
							that.upload_filecontainer_list()
						}
					}

				},
				error: function (model, response) {
					console.log('error: CategoryView-save-FileIndex')
					console.log(response)
				}
			});
		});

		// send message to thread
		while (local_criterion && this.global_criterion) {
			var subfile = file.substring(index*fileindex_length,(index+1)*fileindex_length);
			if (subfile) {
				t.message({subfile: subfile, subfile_len: subfile.length, key: that.key, index: index});
			} else {
				local_criterion = 0;
			}
			index++;
		}
	},

	//////////////////////////////////////////////////////////////// DOWNDLOAD
	dowload_filecontainer: function(event) {
		var that = this;
		var index = $(event.currentTarget).attr('name');
		var filecontainer = this.filecontainer_list[index];
		filecontainer.fetch({
			type: 'GET',
			success: function (model, response) {
				console.log('success: ContainerView-fetch-Category')
				// init
				var name = MyCrypto.decrypt(response.name, that.key)
				var file = MyCrypto.decrypt(response.file, that.key);
				// download
				blobUtil.binaryStringToBlob(file).then(function (blob) {
					saveAs(blob, name);
				}).catch(function (err) {
					console.log(err)
				});
			},
			error: function (model, response) {
				console.log('error: ContainerView-fetch-Category')
				console.log(response)
			}
		});
	},

	//////////////////////////////////////////////////////////////// OTHER
	get_filecontainer_list: function() {
		var that = this;
		that.filecontainer_list = [];
		var filecontainer = new FileContainer({
			id: null,
			category: new Category({id: that.pk_category})
		});
		filecontainer.fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CategoryView-fetch-FileContainer')
				for (var i=0; i<response.length; i++) {
					var filecontainer = new FileContainer(response[i]);
					filecontainer.set('name', MyCrypto.decrypt(filecontainer.get('name'), that.key));
					filecontainer.set('path', MyCrypto.decrypt(filecontainer.get('path'), that.key));
					that.filecontainer_list.push(filecontainer)
				}
				that.search()
			},
			error: function(model, response) {
				console.log('error: CategoryView-fetch-FileContainer')
				console.log(response)
			}
		})
	},

	delete_filecontainer: function (event) {
		var that = this;
		var index = $(event.currentTarget).attr('name');
		var filecontainer = this.filecontainer_list[index];
		filecontainer.destroy({
			success: function(model, response) {
				console.log('success: CategoryView-delete-FileContainer')
				that.get_filecontainer_list()
			},
			error: function(model, response) {
				console.log('error: CategoryView-delete-FileContainer')
				console.log(response)
			}
		})
	},

	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_filecontainer_list = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.filecontainer_list.length; i++) {
				if (this.filecontainer_list[i].get('name').match(re) || this.filecontainer_list[i].get('path').match(re)) {
					this.sub_filecontainer_list.push(this.filecontainer_list[i]);
				}
			}
		} else {
			this.sub_filecontainer_list = this.filecontainer_list;
		}
		this.render_list();
	},

	organise_files: function () {
		this.organized_files = [];
		for (var i=0; i<this.sub_filecontainer_list.length; i++) {

		}
	},

	//////////////////////////////////////////////////////////////// RENDER
	render: function() {
		this.$el.html(this.template({}));
		//this.render_bar();
		//this.render_list();
		//this.render_search();
		//this.render_selected()
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			direcotry_list: this.direcotry_list
			//sub_filecontainer_list: this.sub_filecontainer_list
		}));
	},

	render_bar: function() {
		this.$el.find('#template_bar').html(this.template_bar({
			read_progress_show: this.read_progress_show,
			save_progress_show: this.save_progress_show,
			read_progress_completion : this.read_progress_completion,
			save_progress_completion: this.save_progress_completion
		}));
	},

	render_selected: function() {
		this.$el.find('#template_selected').html(this.template_selected({
			selected_files: this.selected_files
		}));
	},

	render_search: function() {
		this.$el.find('#template_search').html(this.template_search({}));
	},

	render_graph: function() {
		this.$el.find('#visualization')
		console.log("asd", container)
	}

});


$(document).ready(function() {

	var container = document.getElementById('visualization');

	var nodes = new vis.DataSet([
		{id: 1, label: 'Node 1'},
		{id: 2, label: 'Node 2'},
		{id: 3, label: 'Node 3'},
		{id: 4, label: 'Node 4'},
		{id: 5, label: 'Node 5'}
	]);

	// create an array with edges
	var edges = new vis.DataSet([
		{from: 1, to: 3},
		{from: 1, to: 2},
		{from: 2, to: 4},
		{from: 2, to: 5}
	]);

	// provide the data in the vis format
	var data = {
		nodes: nodes,
		edges: edges
	};
	var options = {
		height: '400px',
		width: '400px'
	};

	// initialize your network!
	var network = new vis.Network(container, data, options);

})
