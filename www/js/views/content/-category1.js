window.CategoryView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('category/category'));
		this.template_name = _.template(tpl.get('category/category_name'));
		this.template_path = _.template(tpl.get('category/category_path'));
		this.template_list = _.template(tpl.get('category/category_list'));
		this.template_search = _.template(tpl.get('share/search'));
		$(document).on('keyup', $.proxy(this.search,this));
		$(document).on('keyup', $.proxy(this.save_validation,this));
		this.key = MyCrypto.decrypt(options.authentication.get('password_key'), MyStorage.fetchPassword())
		this.pk_category = options.pk_category;
		this.category = null;
		this.category_name = '';
		this.containers = [];
		this.sub_containers = [];
		this.path = '';
		this.get_category();
		this.get_containers();
	},

	events: {
		"click #update_button": "update_button",
		"click #delete_button": "delete_button",
		"click #add_button": "add_button"
	},

	// CATEGORY
	update_button: function() {
		var that = this;
		var name = $('#name').val();
		that.category.set({'name': MyCrypto.encrypt(name, this.key)})
		that.category.save(null, {
			success: function(model, response) {
				console.log('success: CategoryView-update-Category')
				that.get_category();
			},
			error: function(model, response) {
				console.log('error: CategoryView-update-Category')
				console.log(response)
			}
		})
	},

	delete_button: function() {
		var that = this;
		that.category.destroy({
			success: function(model, response) {
				console.log('success: CategoryView-delete-Category')
				console.log(response)
				Backbone.history.navigate('', true)
			},
			error: function(model, response) {
				console.log('error: CategoryView-delete-Category')
				console.log(response)
			}
		})
	},

	get_category: function() {
		var that = this;
		var category = new Category({id: that.pk_category});
		category.fetch({
			type: 'GET',
			success: function (model, response) {
				console.log('success: ContainerView-fetch-Category')
				that.category = new Category(response);
				that.category_name = MyCrypto.decrypt(that.category.get('name'), that.key);
				that.path = '/' + that.category_name;
				that.save_validation();
				that.render_name();
				that.render_path();
			},
			error: function (model, response) {
				console.log('error: ContainerView-fetch-Category')
				console.log(response)
			}
		});
	},

	// CONTAINERS
	add_button: function() {
		var that = this;
		var name_enc = MyCrypto.encrypt('New Container', this.key);
		var text_enc = MyCrypto.encrypt('', this.key);
		var container = new Container({
			id: null,
			name: name_enc,
			text: text_enc,
			category: new Category({id: this.pk_category, container_set: []}),
			filecontainer_set: []
		});
		container.save(null, {
			success: function(model, response) {
				console.log('success: CategoryView-save-Container')
				that.get_containers()
			},
			error: function(model, response) {
				console.log('error: CategoryView-save-Container')
				console.log(response)
			}
		})
	},

	get_containers: function() {
		var that = this;
		that.containers = [];
		var container = new Container({category: new Category({id: this.pk_category})})
		container.fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CategoryView-fetch-Container')
				for (var i=0; i<response.length; i++) {
					var container = new Container(response[i])
					container.set('name', MyCrypto.decrypt(container.get('name'), that.key));
					that.containers.push({container: container, filecontainer_length: container.get('filecontainer_set').length})
				}
				that.search()
			},
			error: function(model, response) {
				console.log('error: CategoryView-fetch-Container')
				console.log(response)
			}
		})
	},

	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_containers = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.containers.length; i++) {
				if (this.containers[i].container.get('name').match(re)) {
					this.sub_containers.push(this.containers[i]);
				}
			}
		} else {
			this.sub_containers = this.containers;
		}
		this.render_list()
	},

	save_validation: function() {
		var need_to_save = false;
		// name
		if ($('#name').val() != this.category_name) {
			need_to_save = true;
		}

		var id_attr = "#update_button";
		if (need_to_save) {
			$(id_attr).removeClass('btn-success').addClass('btn-warning');
		} else {
			$(id_attr).removeClass('btn-warning').addClass('btn-success');
		}
	},

	render: function() {
		$(this.el).html(this.template({}));
		this.render_name();
		this.render_path();
		this.render_list();
		this.render_search();
		return this;
	},

	render_name: function() {
		this.$el.find('#template_name').html(this.template_name({
			category_name: this.category_name
		}));
	},

	render_path: function() {
		this.$el.find('#template_path').html(this.template_path({
			path: this.path
		}));
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			sub_containers: this.sub_containers
		}));
	},

	render_search: function() {
		this.$el.find('#template_search').html(this.template_search({}));
	}

});
