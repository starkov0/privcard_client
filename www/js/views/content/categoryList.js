window.CategoryListView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('categoryList/main'));
		this.template_list = _.template(tpl.get('categoryList/list'));
		this.template_search = _.template(tpl.get('share/search'));
		$(document).on('keyup', $.proxy(this.search,this));
		this.key = MyCrypto.decrypt(options.authentication.get('password_key'), MyStorage.fetchPassword())
		this.categories = [];
		this.sub_categories = [];
		this.get_categories();
	},

	events: {
		"click #add_category": "add_category"
	},

	add_category: function() {
		var that = this;
		var name = MyCrypto.encrypt('New Category', this.key);
		var category = new Category({id: null, name: name, directory_set: [], filecontainer_set: []})
		category.save(null, {
			success: function(model, response) {
				console.log('success: CardView-save-Card')
				that.get_categories()
			},
			error: function(model, response) {
				console.log('error: CardView-save-Card')
				console.log(response)
			}
		})
	},

	get_categories: function() {
		var that = this;
		that.categories = [];
		that.sub_categories = [];
		new Category().fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CategoryView-fetch-Category')
				for (var i=0; i<response.length; i++) {
					var category = new Category(response[i])
					category.set('name', MyCrypto.decrypt(category.get('name'), that.key));
					that.categories.push({category: category, length: category.get('filecontainer_set').length})
				}
				that.search();
			},
			error: function(model, response) {
				console.log('error: CategoryView-fetch-Category')
				console.log(response)
			}
		})
	},

	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_categories = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.categories.length; i++) {
				if (this.categories[i].category.get('name').match(re)) {
					this.sub_categories.push(this.categories[i]);
				}
			}
		} else {
			this.sub_categories = this.categories;
		}
		this.render_list();
	},

	render: function() {
		$(this.el).html(this.template({}));
		this.render_list()
		this.render_search()
		return this;
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			sub_categories: this.sub_categories
		}));
	},

	render_search: function() {
		this.$el.find('#template_search').html(this.template_search({}));
	}
});
