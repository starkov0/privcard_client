window.CategoryView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('category/main'));
		this.template_name = _.template(tpl.get('category/name'));
		this.template_path = _.template(tpl.get('category/path'));

		// event
		$(document).on('keyup', $.proxy(this.save_validation,this));

		// key
		this.key = MyCrypto.decrypt(options.authentication.get('password_key'), MyStorage.fetchPassword())

		// category
		this.pk_category = options.pk_category;
		this.category = null;
		this.category_name = '';

		// view
		this.content_view = new ContentView({pk_category: this.pk_category, key: this.key})

		this.get_category();
	},

	events: {
		"click #update_button": "update_button",
		"click #delete_button": "delete_button",
		"click #add_button": "add_button"
	},

	update_button: function() {
		var that = this;
		var name = $('#name').val();
		that.category.set({'name': MyCrypto.encrypt(name, this.key)})
		that.category.save(null, {
			success: function(model, response) {
				console.log('success: CategoryView-update-Category')
				that.get_category();
			},
			error: function(model, response) {
				console.log('error: CategoryView-update-Category')
				console.log(response)
			}
		})
	},

	delete_button: function() {
		var that = this;
		that.category.destroy({
			success: function(model, response) {
				console.log('success: CategoryView-delete-Category')
				Backbone.history.navigate('', true)
			},
			error: function(model, response) {
				console.log('error: CategoryView-delete-Category')
				console.log(response)
			}
		})
	},

	get_category: function() {
		var that = this;
		var category = new Category({id: that.pk_category});
		category.fetch({
			type: 'GET',
			success: function (model, response) {
				console.log('success: ContainerView-fetch-Category')
				that.category = new Category(response);
				that.category_name = MyCrypto.decrypt(that.category.get('name'), that.key);
				that.save_validation();
				that.render_name();
				that.render_path();
			},
			error: function (model, response) {
				console.log('error: ContainerView-fetch-Category')
				console.log(response)
			}
		});
	},

	save_validation: function() {
		var need_to_save = false;
		// name
		if ($('#name').val() != this.category_name) {
			need_to_save = true;
		}

		var id_attr = "#update_button";
		if (need_to_save) {
			$(id_attr).removeClass('btn-success').addClass('btn-warning');
		} else {
			$(id_attr).removeClass('btn-warning').addClass('btn-success');
		}
	},

	render: function() {
		$(this.el).html(this.template({}));

		// filecontainer render
		this.content_view.$el = this.$el.find('#template_content');
		this.content_view.render()
		this.content_view.delegateEvents()

		// other render
		this.render_name();
		this.render_path();
		return this;
	},

	render_name: function() {
		this.$el.find('#template_name').html(this.template_name({
			category_name: this.category_name
		}));
	},

	render_path: function() {
		this.$el.find('#template_path').html(this.template_path({
			path: this.category_name
		}));
	}

});
