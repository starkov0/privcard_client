window.ContainerView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('container/container'));
		this.template_name = _.template(tpl.get('container/container_name'));
		this.template_text = _.template(tpl.get('container/container_text'));
		this.template_path = _.template(tpl.get('container/container_path'));
		this.template_list = _.template(tpl.get('container/container_list'));
		this.template_search = _.template(tpl.get('share/search'));
		$(document).on('keyup', $.proxy(this.search,this));
		$(document).on('keyup', $.proxy(this.save_validation,this));
		this.key = MyCrypto.decrypt(options.authentication.get('password_key'), MyStorage.fetchPassword())
		this.container = null;
		this.fileContainers = [];
		this.sub_fileContainers = [];
		this.path = '';
		this.name = '';
		this.text = '';
		this.pk_category = options.pk_category;
		this.pk_container = options.pk_container;
		this.get_container();
		this.get_filecontainers();
	},

	events: {
		"click #update_button": "update_button",
		"click #delete_button": "delete_button",
		"click #upload_filecontainers": "upload_filecontainers",
		"click #dowload_filecontainer": "dowload_filecontainer",
		"click #dowload_filecontainers": "dowload_filecontainers",
		"click #delete_filecontainer": "delete_filecontainer"
	},

	// CONTAINER
	update_button: function() {
		var that = this;
		that.container.set('name', MyCrypto.encrypt($('#name').val(), this.key));
		that.container.set('text', MyCrypto.encrypt($('#text').val(), this.key));
		that.container.save(null, {
			success: function(model, response) {
				console.log('success: ContainerView-save-Container')
				that.get_container()
			},
			error: function(model, response) {
				console.log('error: ContainerView-save-Container')
				console.log(response)
			}
		});
	},

	delete_button: function() {
		var that = this;
		that.container.destroy({
			success: function(model, response) {
				console.log('success: CategoryView-delete-Category')
				Backbone.history.navigate('category/' + that.pk_category, true)
			},
			error: function(model, response) {
				console.log('error: CategoryView-delete-Category')
				console.log(response)
			}
		})
	},

	get_container: function() {
		var that = this;
		that.fileContainers = [];
		that.path = '';
		var container = new Container({
			id: that.pk_container,
			category: new Category({id: that.pk_category})
		});
		container.fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: ContainerView-fetch-Container')
				that.container = new Container(response);
				that.name = MyCrypto.decrypt(that.container.get('name'), that.key);
				that.text = MyCrypto.decrypt(that.container.get('text'), that.key)
				that.path = '/' + MyCrypto.decrypt(that.container.get('category').name, that.key) + '/' + that.name;
				that.save_validation();
				that.render_name();
				that.render_path();
			},
			error: function(model, response) {
				console.log('error: ContainerView-fetch-Container')
				console.log(response)
			}
		})
	},

	// FILECONTAINER
	upload_filecontainers: function() {
		var that = this;
		var category = new Category({id: that.pk_category, container_set: []});
		var container = new Container({id: that.pk_container, category: category, filecontainer_set: []})
		var files = $('#file').prop('files');
		for (var i=0; i<files.length; i++) {
			(function(file_i) {
				var reader = new FileReader();

				//reader.onprogress = function(data) {
				//	console.log(data)
				//	if (data.lengthComputable) {
				//		var progress = parseInt( ((data.loaded / data.total) * 100), 10 );
				//		console.log(progress, data.loaded, data.total);
				//	}
				//};

				reader.onload = function() {

					// post
					var name = MyCrypto.encrypt(file_i.name, that.key);
					that.post_filecontainer(name)

					//var file = reader.result
					//var subfile_length = 100000;
					//var file_indexes = Math.ceil(file.length / subfile_length);
					//var file_index = 0;
					//var continue_criterion = 1;
					//while (continue_criterion) {
					//
					//	var subfile = file.substring(file_index*subfile_length,(file_index+1)*subfile_length);
					//	file_index++;
					//
					//	if (subfile) {
					//		var subfile_enc = MyCrypto.encrypt(subfile, that.key);
					//	} else {
					//		continue_criterion = 0;
					//	}
					//
					//}

					//console.log(file_enc.length)
					//file_enc = file_enc.substring(1,file_enc.length-1)

					//for (var i=0; i<sub_file_nb; i++) {
					//	var subfile = file.substring(i*sub_file_nb,(i+1)*sub_file_nb)
					//	console.log(subfile.length)
					//	var subfile_enc = MyCrypto.encrypt(subfile, that.key);
					//	enc = enc + subfile_enc;
					//}
					//if ()

					//enc = enc.substring(1,enc.length-1)
					////enc = enc + '{';
					//
					//var enc_array = enc.split('}{');
					//console.log(enc_array)
					//
					////var dec = ''
					//for (var i=0; i<enc_array.length; i++) {
					//	var obj = '{'+enc_array[i]+'}'
					//	console.log(MyCrypto.decrypt('{'+enc_array[i]+'}', that.key))
					//
					//}
					//
					//console.log(dec)

					//var name = MyCrypto.encrypt(file_i.name, that.key);
					//var file = MyCrypto.encrypt(reader.result, that.key);
					//console.log("file: " + file.length);

					//var compressed = LZString.compress(file);
					//console.log("compressed: " + compressed.length);

				}

				reader.readAsBinaryString(file_i);
			}(files[i]))
		}
	},

	post_filecontainer: function(name) {
		// init
		var that = this
		var category = new Category({id: that.pk_category, container_set: []});
		var container = new Container({id: that.pk_container, category: category, filecontainer_set: []})
		// object
		var filecontainer = new FileContainer({
			id: null,
			name: name,
			category: category,
			container: container
		});
		// post
		filecontainer.save(null, {
			success: function(model, response) {
				console.log('success: ContainerView-save-FileContainer')
				return response.id
			},
			error: function(model, response) {
				console.log('error: ContainerView-save-FileContainer')
				console.log(response)
			}
		})
	},

	put_filecontainer: function() {

	},

	//// FILECONTAINER
	//upload_filecontainers: function() {
	//	var that = this;
	//	var category = new Category({id: that.pk_category, container_set: []});
	//	var container = new Container({id: that.pk_container, category: category, filecontainer_set: []})
	//	var files = $('#file').prop('files');
	//	for (var i=0; i<files.length; i++) {
	//		(function(file_i) {
	//			var reader = new FileReader();
	//			reader.onload = function(e) {
	//				// init
	//				var name = MyCrypto.encrypt(file_i.name, that.key);
	//				var file = MyCrypto.encrypt(reader.result, that.key);
	//
	//
	//				var compressed = LZString.compress(file);
	//				console.log("reader.result: " + reader.result.length);
	//				console.log("file: " + file.length);
	//				console.log("compressed: " + compressed.length);
	//
	//
	//				var filecontainer = new FileContainer({
	//					id: null,
	//					name: name,
	//					file: compressed,
	//					category: category,
	//					container: container
	//				});
	//				// save
	//				//filecontainer.save(null, {
	//				//	success: function(model, response) {
	//				//		console.log('success: ContainerView-save-FileContainer')
	//				//		that.get_filecontainers()
	//				//	},
	//				//	error: function(model, response) {
	//				//		console.log('error: ContainerView-save-FileContainer')
	//				//		console.log(response)
	//				//	}
	//				//})
	//			}
	//			reader.readAsBinaryString(file_i);
	//		}(files[i]))
	//	}
	//},

	dowload_filecontainer: function(event) {
		var that = this;
		var index = $(event.currentTarget).attr('name');
		var filecontainer = this.fileContainers[index];
		filecontainer.fetch({
			type: 'GET',
			success: function (model, response) {
				console.log('success: ContainerView-fetch-Category')
				// init
				var name = MyCrypto.decrypt(response.name, that.key)
				var file = MyCrypto.decrypt(response.file, that.key);
				// download
				blobUtil.binaryStringToBlob(file).then(function (blob) {
					saveAs(blob, name);
				}).catch(function (err) {
					console.log(err)
				});
			},
			error: function (model, response) {
				console.log('error: ContainerView-fetch-Category')
				console.log(response)
			}
		});
	},

	dowload_filecontainers: function() {
		var that = this;
		for (var i=0; i<this.fileContainers.length; i++) {
			var filecontainer = this.fileContainers[i];
			filecontainer.fetch({
				type: 'GET',
				success: function (model, response) {
					console.log('success: ContainerView-fetch-Category')
					// init
					var name = MyCrypto.decrypt(response.name, that.key)
					var file = MyCrypto.decrypt(response.file, that.key);
					// download
					blobUtil.binaryStringToBlob(file).then(function (blob) {
						saveAs(blob, name);
					}).catch(function (err) {
						console.log(err)
					});
				},
				error: function (model, response) {
					console.log('error: ContainerView-fetch-Category')
					console.log(response)
				}
			});
		}
	},

	delete_filecontainer: function(event) {
		var that = this;
		var index = $(event.currentTarget).attr('name');
		var filecontainer = this.fileContainers[index];
		filecontainer.destroy({
			success: function(model, response) {
				console.log('success: ContainerView-delete-FileContainer')
				that.get_filecontainers()
			},
			error: function(model, response) {
				console.log('error: ContainerView-delete-FileContainer')
				console.log(response)
			}
		})
	},

	get_filecontainers: function() {
		var that = this;
		that.fileContainers = [];
		var category = new Category({id: that.pk_category});
		var container = new Container({id: that.pk_container, category: category})
		var filecontainer = new FileContainer({
			category: category,
			container: container
		});
		filecontainer.fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: ContainerView-fetch-FileContainer')
				for (var i=0; i<response.length; i++) {
					var filecontainer = new FileContainer(response[i]);
					filecontainer.set('name', MyCrypto.decrypt(filecontainer.get('name'), that.key));
					that.fileContainers.push(filecontainer)
				}
				console.log(that.fileContainers)
				that.search()
			},
			error: function(model, response) {
				console.log('error: ContainerView-fetch-FileContainer')
				console.log(response)
			}
		})
	},

	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_fileContainers = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.fileContainers.length; i++) {
				if (this.fileContainers[i].get('name').match(re)) {
					this.sub_fileContainers.push(this.fileContainers[i]);
				}
			}
		} else {
			this.sub_fileContainers = this.fileContainers;
		}
		this.render_list();
	},

	save_validation: function() {
		var need_to_save = false;
		// name
		if ($('#name').val() != this.name) {
			need_to_save = true;
		}
		// text
		if ($('#text').val() != this.text) {
			need_to_save = true;
		}
		var id_attr = "#update_button";
		if (need_to_save) {
			$(id_attr).removeClass('btn-success').addClass('btn-warning');
		} else {
			$(id_attr).removeClass('btn-warning').addClass('btn-success');
		}
	},

	render: function() {
		$(this.el).html(this.template({}));
		this.render_name();
		this.render_text();
		this.render_path();
		this.render_search();
		this.render_list();
		return this;
	},

	render_name: function() {
		this.$el.find('#template_name').html(this.template_name({
			name: this.name
		}));
	},

	render_text: function() {
		this.$el.find('#template_text').html(this.template_text({
			text: this.text
		}));
	},

	render_path: function() {
		this.$el.find('#template_path').html(this.template_path({
			path: this.path
		}));
	},

	render_search: function() {
		this.$el.find('#template_search').html(this.template_search({}));
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			sub_fileContainers: this.sub_fileContainers
		}));
	}

});
