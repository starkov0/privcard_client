window.CardView = Backbone.View.extend({

	initialize: function(options) {

		// templates
		this.template = _.template(tpl.get('card/main'));
		this.template_list = _.template(tpl.get('card/category_list'));
		this.template_search = _.template(tpl.get('share/search'));
		this.template_update_button = _.template(tpl.get('share/update_button'));

		// event
		$(document).on('keyup', $.proxy(this.search,this));
		$(document).on('keyup', $.proxy(this.save_validation,this));

		// account
		this.account = options.account;
		this.key = MyCrypto.decrypt(this.account.get('password_key'), MyStorage.fetchPassword())

		// card
		this.pk_card = options.pk_card;
		this.card = null;
		this.card_name = '';
		this.card_username = '';
		this.card_password = '';

		// categories
		this.category_list = [];
		this.initial_category_list = [];
		this.sub_category_list = [];

		// fetch
		this.get_category_list();
	},

	events: {
		"click #update_button": "update_button",
		"click #delete_button": "delete_button",
		'click [type="checkbox"]':'click_checkbox'
	},

	click_checkbox: function (event) {
		var index = $(event.currentTarget).attr('name');
		this.category_list[index].is_checked = !this.category_list[index].is_checked;
		this.save_validation()
	},

	update_button: function() {
		var that = this;

		// init array
		var category_set = [];
		for (var i=0; i<that.category_list.length; i++){
			if ($('#sub_category_list'+i + ' input').prop('checked')) {
				category_set.push(new Category({
					id: that.category_list[i].category.id,
					directory_set: [],
					filecontainer_set: []
				}))
			}
		}

		// encrypt card
		this.card.set('name', MyCrypto.encrypt($('#name').val(), this.key));
		this.card.set('category_set', category_set)

		// save
		that.card.save(null, {
			success: function(model, response) {
				console.log('success: CardView-update-Card')
				that.get_card();
			},
			error: function(model, response) {
				console.log('error: CardView-update-Card')
				console.log(response)
			}
		})
	},

	delete_button: function() {
		var that = this;

		// destroy
		that.card.destroy({
			success: function(model, response) {
				console.log('success: CardView-delete-Card')

				// go back to card list
				Backbone.history.navigate('card', true)
			},
			error: function(model, response) {
				console.log('error: CardView-delete-Card')
				console.log(response)
			}
		})
	},

	// get all categories
	get_category_list: function() {
		var that = this;

		// init array
		this.category_list = [];

		// fetch from server
		new Category().fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CategoryView-fetch-Category')
				for (var i=0; i<response.length; i++) {
					var category = new Category(response[i])
					category.set('name', MyCrypto.decrypt(category.get('name'), that.key));
					that.category_list.push({
						category: category, is_checked: false,
						length: category.get('filecontainer_set').length
					});
					that.initial_category_list.push({is_checked: false})
				}

				// get card
				that.get_card()
			},
			error: function(model, response) {
				console.log('error: CategoryView-fetch-Category')
				console.log(response)
			}
		})
	},

	// get card and check which category is related to the card
	get_card: function() {
		var that = this;

		// reset card-categories relationship
		for (var j=0; j<that.category_list.length; j++) {
			that.category_list[j].is_checked = false;
		}

		// fetch card
		var card = new Card({id: this.pk_card});
		card.fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CardView-fetch-Card')

				// decrypt card
				that.card = new Card(response);
				that.card_name = MyCrypto.decrypt(that.card.get('name'), that.key);
				that.card_username = that.card.get('username');
				that.card_password = MyCrypto.decrypt(that.card.get('key_password'), that.key)

				// decrypt categories and check card-categories relationships
				for (var i=0; i<that.card.get('category_set').length; i++) {
					for (var j=0; j<that.category_list.length; j++) {
						if (that.card.get('category_set')[i].id == that.category_list[j].category.id) {
							that.category_list[j].is_checked = true;
							that.initial_category_list[j].is_checked = true;
						}
					}
				}

				// check search bar
				that.save_validation()
				that.search();
			},
			error: function(model, response) {
				console.log('error: CardView-fetch-Card')
				console.log(response)
			}
		})
	},

	// regex search from search bar
	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_category_list = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.category_list.length; i++) {
				if (this.category_list[i].category.get('name').match(re)) {
					this.sub_category_list.push(this.category_list[i]);
				}
			}
		} else {
			this.sub_category_list = this.category_list;
		}
		this.render_list();
	},

	// check if needs to save
	save_validation: function() {
		var need_to_save = false;
		// name
		if ($('#name').val() != this.card_name) {
			need_to_save = true;
		}
		// categories
		for (var i=0; i<this.category_list.length; i++){
			console.log(this.initial_category_list[i], this.category_list[i])
			if (this.initial_category_list[i].is_checked != this.category_list[i].is_checked) {
				need_to_save = true
			}
		}
		var id_attr = "#update_button";
		if (need_to_save) {
			$(id_attr).removeClass('btn-success').addClass('btn-warning');
		} else {
			$(id_attr).removeClass('btn-warning').addClass('btn-success');
		}
	},

	render: function() {
		$(this.el).html(this.template({
			card_name: this.card_name,
			card_username: this.card_username,
			card_password: this.card_password
		}));
		this.render_list();
		this.render_search();
		this.render_update_button()
		return this;
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			sub_category_list: this.sub_category_list
		}));
	},

	render_search: function() {
		this.$el.find('#template_search').html(this.template_search({}));
	},

	render_update_button: function() {
		this.$el.find('#template_update_button').html(this.template_update_button({}));
	}

});
