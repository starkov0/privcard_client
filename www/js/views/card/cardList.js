window.CardListView = Backbone.View.extend({

	initialize: function(options) {
		this.template = _.template(tpl.get('cardList/main'));
		this.template_list = _.template(tpl.get('cardList/card_list'));
		this.account = options.account;
		this.key = MyCrypto.decrypt(this.account.get('password_key'), MyStorage.fetchPassword())
		this.card_list = [];
		this.category_list = [];
		this.get_card_list();
	},

	events: {
		"click #add_card": "add_card",
		"click #goto_card": "goto_card"
	},

	add_card: function() {
		this.add_card_help(1)
	},

	add_card_help: function(random_string_lenght) {
		var that = this;
		var password = MyRandomGenerator.generate_random_string(22);
		var card = new Card({
			id: null,
			name: MyCrypto.encrypt('New Card', this.key),
			username: that.account.get('username') + '_' + MyRandomGenerator.generate_random_string(random_string_lenght),
			password: MyCrypto.generate_hash(password),
			password_key: MyCrypto.encrypt(this.key, password), // key used by the card user
			key_password: MyCrypto.encrypt(password, this.key), // password shown to the account user
			category_set: []
		});
		card.save(null, {
			success: function(model, response) {
				console.log('success: CardView-save-Card')
				that.get_card_list()
			},
			error: function(model, response) {
				console.log('error: CardView-save-Card')
				console.log(response)
				that.add_card_help(random_string_lenght+1)
			}
		})
	},

	goto_card: function(event) {
		var index = $(event.currentTarget).attr('data-name');
		var card = this.card_list[index]
		Backbone.history.navigate('card/'+card.id, true)
	},

	get_card_list: function() {
		var that = this;
		that.card_list = [];
		new Card().fetch({
			type: 'GET',
			success: function(model, response) {
				console.log('success: CardView-fetch-card_list')
				for (var i=0; i<response.length; i++) {
					var card = new Card(response[i])
					card.set('name', MyCrypto.decrypt(card.get('name'), that.key));
					card.set('key_password', MyCrypto.decrypt(card.get('key_password'), that.key));
					for (var j=0; j<card.get('category_set').length; j++) {
						var category = new Category(card.get('category_set')[j]);
						category.set('name', MyCrypto.decrypt(category.get('name'), that.key))
						that.category_list.push({category: category, length: category.get('filecontainer_set').length})
						card.get('category_set')[j].name = MyCrypto.decrypt(card.get('category_set')[j].name, that.key);
					}
					that.card_list.push(card)
				}
				that.render()
			},
			error: function(model, response) {
				console.log('error: CardView-fetch-card_list')
				console.log(response)
			}
		})
	},

	search: function() {
		var re_str = $('#search').val();
		if (re_str) {
			this.sub_category_list = [];
			var re = new RegExp(re_str,'i');
			for (var i=0; i<this.category_list.length; i++) {
				if (this.category_list[i].category.get('name').match(re)) {
					this.sub_category_list.push(this.category_list[i]);
				}
			}
		} else {
			this.sub_category_list = this.category_list;
		}
		this.render_list();
	},

	render: function() {
		$(this.el).html(this.template({
			card_list: this.card_list
		}));
		this.render_list()
		return this;
	},

	render_list: function() {
		this.$el.find('#template_list').html(this.template_list({
			category_list: this.category_list
		}));
	}

});
