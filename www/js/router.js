var AppRouter = Backbone.Router.extend({

	initialize: function() {
		//Initialize the publish/subscribe bus
		window.pubSubBus = _.extend({}, Backbone.Events);
		this.headerView = new HeaderView();
		this.footerView = new FooterView();
		this.showViewFooter(this.headerView);
		this.showViewHeader(this.footerView);
	},

	routes: {
		'' : 'index',
		'register': 'register',
		'login': 'login',
		'account': 'account',
		'updatePassword': 'updatePassword',
		'card': 'cardList',
		'card/:pk_card': 'card',
		'squestion': 'squestionList',
		'category/:pk_category': 'category'
	},

	// ACCESS
	register: function() {
		if (MyStorage.fetchAuthenticationType()) {
			Backbone.history.navigate('', true)
		} else {
			this.anonymousMode()
			app.showViewContent(new RegisterView());
		}
	},
	login: function() {
		if (MyStorage.fetchAuthenticationType()) {
			Backbone.history.navigate('', true)
		} else {
			this.anonymousMode()
			app.showViewContent(new LoginView());
		}
	},
	updatePassword: function() {
		if (MyStorage.fetchAuthenticationType()) {
			Backbone.history.navigate('', true)
		} else {
			this.anonymousMode()
			app.showViewContent(new UpdatePassword());
		}
	},

	// ACCOUNT
	account: function() {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function(model, response) {
					console.log('success: AccountView-fetch-Account');
					app.showViewContent(new AccountView({account: new Account(response)}));
				},
				error : function (model, response) {
					console.log('error: AccountView-fetch-Account');
					console.log(response)
				}
			})
		} else {
			Backbone.history.navigate('', true)
		}
	},
	card: function(pk_card) {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function(model, response) {
					console.log('success: CardView-fetch-Account');
					app.showViewContent(new CardView({account: new Account(response), pk_card: pk_card}));
				},
				error : function (model, response) {
					console.log('error: CardView-fetch-Account');
					console.log(response)
				}
			})
		} else {
			Backbone.history.navigate('', true)
		}
	},
	cardList: function() {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function(model, response) {
					console.log('success: CardListView-fetch-Account');
					app.showViewContent(new CardListView({account: new Account(response)}));
				},
				error : function (model, response) {
					console.log('error: CardListView-fetch-Account');
					console.log(response)
				}
			})
		} else {
			Backbone.history.navigate('', true)
		}
	},
	squestionList: function() {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function(model, response) {
					console.log('success: SQuestionListView-fetch-Account');
					app.showViewContent(new SQuestionListView({account: new Account(response)}));
				},
				error : function (model, response) {
					console.log('error: SQuestionListView-fetch-Account');
					console.log(response)
				}
			})
		} else {
			Backbone.history.navigate('', true)
		}
	},

	// CONTENT
	index: function() {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function(model, response) {
					console.log('success: CategoryListView-fetch-Account');
					app.showViewContent(new CategoryListView({authentication: new Account(response)}));
				},
				error : function (model, response) {
					console.log('error: CategoryListView-fetch-Account');
					console.log(response)
				}
			})
		} else if (MyStorage.fetchAuthenticationType() == 'card') {
			this.cardMode()
			new Card().fetch({
				success: function(model, response) {
					console.log('success: CategoryListView-fetch-Card');
					app.showViewContent(new CategoryListView({authentication: new Card(response)}));
				},
				error : function (model, response) {
					console.log('error: CategoryListView-fetch-Card');
					console.log(response)
				}
			})
		} else {
			this.anonymousMode()
			app.showViewContent(new IndexView());
		}
	},
	category: function(pk_category) {
		if (MyStorage.fetchAuthenticationType() == 'account') {
			this.accountMode()
			new Account().fetch({
				success: function (model, response) {
					console.log('success: CategoryView-fetch-Account');
					app.showViewContent(new CategoryView({authentication: new Account(response),
						pk_category: pk_category}));
				},
				error: function (model, response) {
					console.log('error: ContainerView-fetch-Account');
					console.log(response)
				}
			})
		} else if (MyStorage.fetchAuthenticationType() == 'card') {
			this.cardMode()
			new Card().fetch({
				success: function (model, response) {
					console.log('success: CategoryView-fetch-Account');
					app.showViewContent(new CategoryView({authentication: new Card(response),
						pk_category: pk_category}));
				},
				error: function (model, response) {
					console.log('error: ContainerView-fetch-Account');
					console.log(response)
				}
			})
		} else {
			Backbone.history.navigate('', true)
		}
	},

	// SHOW VIEW
	showViewContent: function(view) {
		if (this.currentViewContent)
			this.currentViewContent.close(); //on detatch l'ancienne view
		$('#content').html(view.render().el); //Affiche la nouvelle
		this.currentViewContent = view;
		return view;
	},
	showViewHeader: function(view) {
		if (this.currentViewHeader)
			this.currentViewHeader.close(); //on detatch l'ancienne view
		$('#header').html(view.render().el); //Affiche la nouvelle
		this.currentViewHeader = view;
		return view;
	},
	showViewFooter: function(view) {
		if (this.currentViewFooter)
			this.currentViewFooter.close(); //on detatch l'ancienne view
		$('#footer').html(view.render().el); //Affiche la nouvelle
		this.currentViewFooter = view;
		return view;
	},

	// TOKEN
	setToken: function() {
		Backbone.$.ajaxSetup({
			headers: {'Authorization' :'Token ' + MyStorage.fetchToken()}
		});
	},
	removeToken: function() {
		Backbone.$.ajaxSetup({
			headers: ''
		});
	},

	// MODE
	anonymousMode: function() {
		this.removeToken()
		this.headerView.setAnonymous()
	},
	accountMode: function() {
		this.setToken()
		this.headerView.setAccount()
	},
	cardMode: function() {
		this.setToken()
		this.headerView.setCard()
	}

});

