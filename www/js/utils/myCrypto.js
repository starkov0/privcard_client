/**
 * Created by pierrestarkov on 08/08/15.
 */

MyCrypto = {};

MyCrypto.generate_hash = function(message) {
	return sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(message));
};

MyCrypto.encrypt = function(message, key) {
	return sjcl.encrypt(key, message)
};

MyCrypto.decrypt = function(message, key) {
	return sjcl.decrypt(key, message);
};