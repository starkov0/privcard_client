/**
 * Created by pierrestarkov on 11/08/15.
 */
MyRandomGenerator = {};
MyRandomGenerator.generate_random_string = function(string_length) { // 2^128 is equal to 22 random caracters from generate_random_string
	var text = "";
	var possible = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789";
	for( var i=0; i < string_length; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
};