/**
 * Created by pierrestarkov on 08/08/15.
 */

MyStorage = {};

MyStorage.store = function(name, obj) {
	window.localStorage.setItem(name, obj);
};

MyStorage.fetch = function(name) {
	return window.localStorage.getItem(name);
};

MyStorage.fetchAuthenticationType = function() {
	return this.fetch('authenticationType');
};

MyStorage.fetchToken = function() {
	return this.fetch('token');
};

MyStorage.fetchPassword = function() {
	return this.fetch('password');
};

MyStorage.storeAuthenticationType = function(authenticationType) {
	return this.store('authenticationType',authenticationType);
};

MyStorage.storeToken = function(token) {
	return this.store('token',token);
};

MyStorage.storePassword = function(password) {
	return this.store('password',password);
};
