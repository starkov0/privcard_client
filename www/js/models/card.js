window.Card = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix,

	url: function() {
		var url = this.urlRoot;

		if (MyStorage.fetchAuthenticationType() == 'account') {
			url += 'account/card'
		} else if (MyStorage.fetchAuthenticationType() == 'card') {
			url += 'card'
		}

		// id
		if (this.id) {
			url += '/' + this.id
		}

		return url;
	},

	//Attributes; sending to the server
	defaults: {
		name: '',
		username: '',
		password: '',
		password_key: '',
		key_password: ''
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

