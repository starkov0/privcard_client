window.Anonymous = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "anonymous/" ,

	url: function() {
		return this.urlRoot + this.get('sub_url') + this.get('username');
	},

	//Attributes; sending to the server
	defaults: {
		sub_url: 'username/',
		username: 'asd',
		answer: '',
		password: '',
		password_key: ''
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

