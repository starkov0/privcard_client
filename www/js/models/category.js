window.Category = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix,

	url: function() {
		var url = this.urlRoot + MyStorage.fetchAuthenticationType() +
			'/category';

		// id
		if (this.id) {
			url += '/' + this.id
		}

		return url;
	},

	defaults: {
		name: ''
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

