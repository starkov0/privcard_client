window.SQuestion = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "account/squestion",

	//Attributes; sending to the server
	defaults: {
		question: '',
		answer: '', // to hash
		answer_key: '' // to encrypt
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

