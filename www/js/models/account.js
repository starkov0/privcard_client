window.Account = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "account",

	//Attributes; sending to the server
	defaults: {
		username: '',
		password: '', // to hash
		password_key: '', // to encrypt
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

