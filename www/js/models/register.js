window.Register = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "register",

	//Attributes; sending to the server
	defaults: {
		username: '',
		password: '',
		password_key: ''
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});