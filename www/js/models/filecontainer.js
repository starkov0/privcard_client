window.FileContainer = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix,

	url: function() {
		var url = this.urlRoot + MyStorage.fetchAuthenticationType() +
			'/category/' + this.get('category').id +
			'/filecontainer';

		if (this.id) {
			url += '/' + this.id
		}

		return url;
	},

	defaults: {
		name: '',
		path: '',
		category: null
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

