window.Logout = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "logout",

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});